const theme = require('../theme');

export default {
  history: 'hash',
  treeShaking: true,
  define: {
    'process.env.API_PROTOCOL': process.env.API_PROTOCOL,
    'process.env.API_HOST': process.env.API_HOST,
    'process.env.API_PORT': process.env.API_PORT,
    'process.env.API_PREFIX': process.env.API_PREFIX,
  },
  routes: [
    {
      path: '/',
      component: '../layouts/GlobalLayout',
      routes: [{ path: '/', component: '../pages/Index/Index', exact: true },
      { path: '/register', component: '../pages/Register/Register', exact: true },
      { path: '/member', component: '../pages/Member/Member', exact: true },
      { path: '/memberdata', component: '../pages/Memberdata/Memberdata', exact: true },
      { path: '/reviewer', component: '../pages/Reviewer/Reviewer', exact: true },
      { path: '/upload', component: '../pages/Upload/Upload', exact: true },
      { path: '/start', component: '../pages/Start/Start', exact: true },
      { path: '/finalpaper', component: '../pages/FinalPaper/FinalPaper', exact: true },
      { path: '/forgot', component: '../pages/Forgot/Forgot', exact: true },
      { path: '/checkEmail', component: '../pages/CheckEmail/CheckEmail', exact: true },
      { path: '/verifySuccess', component: '../pages/VerifySuccess/VerifySuccess', exact: true },
      { path: '/verifyError', component: '../pages/VerifyError/VerifyError', exact: true },
      { path: '/verifyDone', component: '../pages/VerifyDone/VerifyDone', exact: true },
      { path: '/changePassword', component: '../pages/ChangePassword/ChangePassword', exact: true }
    ],
      
    },
  ],
  hash: true,
  theme: theme,
  disableCSSModules: true,
  lessLoaderOptions: {
    modifyVars: theme,
    javascriptEnabled: true,
  },
  plugins: [
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: true,
        title: 'umi-test',
        dll: false,
        routes: {
          exclude: [
            /models\//,
            /services\//,
            /model\.(t|j)sx?$/,
            /service\.(t|j)sx?$/,
            /components\//,
          ],
        },
      },
    ],
  ],
};
