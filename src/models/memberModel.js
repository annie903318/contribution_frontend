import { message } from 'antd';
import { routerRedux } from 'dva';
import { POST_login, POST_register, POST_reset, GET_user, PATCH_user, PATCH_password  } from "../services/memberService";


export default {
  namespace: "member",

  state: {},

  effects: {
    *POST_login({ payload,callback, loading }, { put, call, select }) {
      // 使用者登入
      try {
        const response = yield call(POST_login,payload);
        yield put({ type: "SAVE_login", payload: response });
        if(response.token){
          localStorage.setItem('token', response.token);
          message.success(response.message);
          // 導向投稿者頁面
          yield put(routerRedux.push('/member'));
        }        
        
      } catch (err) {
        message.error(err.response.data.message);
      }
    },
    *POST_register({ payload,callback, loading }, { put, call, select }) {
      // 使用者註冊
      try {
        const response = yield call(POST_register,payload);
        yield put({ type: "SAVE_register", payload: response.message });
        message.success(response.message);
        yield put(routerRedux.push('/checkEmail'));

      } catch (err) {
        message.error(err.response.data.message);
      }
    },
    *POST_reset({ payload,callback, loading }, { put, call, select }) {
      // 忘記密碼
      try {
        const response = yield call(POST_reset,payload);
        message.success(response.message);
        yield put({ type: "SAVE_reset", payload: response.message }); 
        yield put(routerRedux.push('/'));       

      } catch (err) {
        message.error(err.response.data.message);
      }
    },
    * logout ({callback, loading }, {put, call, select}){
      try{
        // 使用者登出
        localStorage.removeItem('token');
        // 導向登入頁面
        yield put(routerRedux.push('/'));
      }catch(err){
        console.log(err);
      }
    },
    *GET_user({ callback, loading }, { put, call, select }) {
      try {
        //使用者個人資料
        const response = yield call(GET_user);
        yield put({ type: "SAVE_user", payload: response });
      } catch (err) {
        console.log(err);
      }
    },
    *PATCH_user({ payload,callback, loading }, { put, call, select }) {
      try {
        //修改使用者個人資料
        const response = yield call(PATCH_user,payload);
        yield put({ type: "SAVE_userEdit", payload: response });
        message.success(response.message);
      } catch (err) {
        message.error(err.response.data.message);
      }
    },
    *PATCH_password({ payload,callback, loading }, { put, call, select }) {
      try {
        //修改密碼
        const response = yield call(PATCH_password,payload);
        yield put({ type: "SAVE_password", payload: response });
        message.success(response.message);
      } catch (err) {
        message.error(err.response.data.message);
      }
    },
  
    
  },

  reducers: {
    SAVE_login(state, { payload }) {
      return {
        ...state,
        loginRes: payload,
      };
    },
    SAVE_register(state, { payload }) {
      return {
        ...state,
        registerRes: payload,
      };
    },
    SAVE_reset(state, { payload }) {
      return {
        ...state,
        resetRes: payload,
      };
    },
    SAVE_user(state, { payload }) {
      return {
        ...state,
        userRes: payload,
      };
    },
    SAVE_userEdit(state, { payload }) {
      return {
        ...state,
        userEditRes: payload,
      };
    },
    
  },
};
