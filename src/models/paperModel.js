import { routerRedux } from 'dva';
import { GET_subject, POST_paper, GET_personal } from "../services/paperService";

export default {
  namespace: "papers",

  state: {},

  effects: {
    *GET_subject({ callback, loading }, { put, call, select }) {
      try {
        const response = yield call(GET_subject);
        yield put({ type: "SAVE_subject", payload: response.subject });

      } catch (err) {
        console.log(err);
      }
    },
    *POST_paper({ payload,callback, loading }, { put, call, select }) {
      // 新增論文
      try {
        const response = yield call(POST_paper,payload);
        yield put({ type: "SAVE_paper", payload: response.message });
        
      } catch (err) {
        console.log(err);
      }
    },
    *GET_personal({ callback, loading }, { put, call, select }) {
      // 個人投稿論文
      try {
        const response = yield call(GET_personal);
        yield put({ type: "SAVE_personal", payload: response.paper });

      } catch (err) {
        console.log(err);
      }
    },
  },

  reducers: {
    SAVE_subject(state, { payload }) {
      return {
        ...state,
        subject: payload,
      };
    },
    SAVE_paper(state, { payload }) {
      return {
        ...state,
        paperRes: payload,
      };
    },
    SAVE_personal(state, { payload }) {
      return {
        ...state,
        personalRes: payload,
      };
    },
  },
};
