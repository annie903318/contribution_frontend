/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Index.less';
import { Row, Col } from 'antd';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
// import logo from '@/assets/images/NUTC_logo.png'
 

const mapStateToProps = (state) => {
  return {
		loginRes: state.member.loginRes
	};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
    POST_login(payload,callback,loading) {
			dispatch({ type: 'member/POST_login', payload,callback,loading});
		},
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Index extends Component {
    state = {
      email: "",
      password: "",
    };

    render() {

      const onFinish = (values) => {
        this.setState({
          email: values.username,
          password: values.password,
        });
        // 使用者登入
        const { POST_login } = this.props;
        POST_login(this.state, null);
      };

     

      return <div className="index" >
        <Row className="indexInner" gutter={[48, 32]} type="flex" align="middle">
          <Col xs={24} md={12} className="left">
            <h2>歡迎來到本投稿網站</h2>
            首先，使用您的用戶名和密碼登錄。<br/>
            如果不確定您是否擁有帳戶或忘記密碼，請轉到“忘記密碼”頁面。<br/>
        
            
          </Col>
          <Col xs={24} md={12} className="right">
            {/* <div className="logo">
              <img src={logo} alt="logo"/>
            </div> */}

              <Form
                name="normal_login"
                className="login-form"
                initialValues={{
                  remember: true,
                }}
                onFinish={onFinish}
              >
                <Form.Item
                  name="username"
                  rules={[
                    {
                      required: true,
                      message: '請輸入帳號!',
                    },
                    {
                      type: 'email',
                      message: '帳號格式不正確!',
                    },
                  ]}
                >
                  <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="帳號" />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: '請輸入密碼！',
                    },
                  ]}
                >
                  <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="密碼"
                  />
                </Form.Item>
                <Form.Item>
                  <a className="login-form-forgot" href="/#/forgot">
                    忘記密碼
                  </a>
                  &nbsp; or &nbsp;<a href="/#/register">註冊</a>
                </Form.Item>

                <Form.Item className="login">
                  <Button type="primary" htmlType="submit" className="login-form-button">
                    登入
                  </Button>
                  
                </Form.Item>
              </Form>
              
          </Col>
        </Row>

      

        
      </div>;
    }
  },
);
