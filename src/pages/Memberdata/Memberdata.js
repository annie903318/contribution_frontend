/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Memberdata.less';
import { Form, Input, Button, Select } from 'antd';
const { Option } = Select;


const mapStateToProps = (state) => {
  return {
		userRes: state.member.userRes || [],
    userEditRes: state.member.userRes || []
	};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
    GET_user(callback,loading) {
			dispatch({ type: 'member/GET_user',callback,loading});
		},
    PATCH_user(payload,callback,loading) {
			dispatch({ type: 'member/PATCH_user',payload,callback,loading});
		},
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Memberdata extends Component {
    formRef = React.createRef();

    componentDidMount = () => {
      const { GET_user } = this.props;
      const loading = (bool) => this.setState({ loading: bool });
      GET_user(null, loading);

      //緩衝表單預設資料
      this.dataLoading();
    };

    dataLoading(){
      setTimeout(()=>{
        //表單預設值
        const {userRes}  = this.props;

        this.formRef.current.setFieldsValue({
          email: userRes.user.email,
          engName: userRes.user.profile.americanName,
          chiName:userRes.user.profile.asianName,
          tel: userRes.user.profile.phone,
          unit:userRes.user.profile.organization,
          job:userRes.user.profile.title
        });
      },100)
    }

    state = {
      title: "",
      organization: "",
      asianName: "",
      americanName: "",
      phone: ""
    };

    render() {     
      const layout = {
        labelCol: {
          xs: { span: 8 },
          md: { span: 8 }, 
      },
        wrapperCol: { 
          xs: { span: 16 },
          md: { span: 16 }, },
      };
     
      const onFinish = (values) => {
        this.setState({
          title: values.job,
          organization: values.unit,
          asianName: values.chiName,
          americanName: values.engName,
          phone: values.tel
        });

        // 修改使用者個人資料
        const { PATCH_user } = this.props;
        PATCH_user(this.state, null);
      };



      return <div className="memberdata" >
     
          <Form {...layout} className="memberdataForm" name="nest-messages" onFinish={onFinish} ref={this.formRef}>

          <Form.Item
                  name="email"
                  label="帳號"
                  tooltip="請輸入有效信箱"
                  style={{
                    width: '50%',
                  }}
                  rules={[
                    {
                      type: 'email',
                      message: '帳號格式不正確!',
                    },
                    {
                      required: true,
                      message: '帳號尚未輸入!',
                    },
                  ]}
                >
                  <Input disabled/>
                </Form.Item>

                <Form.Item
                  name="engName"
                  label="英文姓名"
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '英文姓名尚未輸入!',

                    },
                    {
                      pattern: /^[A-Za-z]+$/,
                      message: '請輸入英文!'
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="chiName"
                  label="中文姓名"
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '中文姓名尚未輸入!',

                    },
                    {
                      pattern: /^[\u4e00-\u9fa5]+$/,
                      message: '請輸入中文!'
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="tel"
                  label="電話"
                  hasFeedback
                  style={{
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '電話尚未輸入!',

                    },
                    {
                      pattern: /^[0-9]+$/,
                      message: '請輸入0~9!'
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="unit"
                  label="單位"
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '單位尚未輸入!',

                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="job"
                  label="稱謂"
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '稱謂尚未選擇!',

                    },
                  ]}
                >
                  <Select>
                    <Option value="學生">學生</Option>
                    <Option value="教授">教授</Option>
                    <Option value="副教授">副教授</Option>
                    <Option value="助理教授">助理教授</Option>
                  </Select>
                </Form.Item>   


                <Form.Item
                  className="submitRow"
                >
                  <Button type="primary" htmlType="submit"  className="submitBtn">
                    修改
                  </Button>
                </Form.Item>
               


          </Form>  


      

        
      </div>;
    }
  },
);
