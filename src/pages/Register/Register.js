import React, { Component } from 'react';
import { connect } from 'dva';
import './Register.less';
import { Form, Input, Button, Select } from 'antd';
const { Option } = Select;


const mapStateToProps = (state) => {
  return {
		registerRes: state.member.registerRes
	};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
    POST_register(payload,callback,loading) {
			dispatch({ type: 'member/POST_register', payload,callback,loading});
		},
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Register extends Component {
    state = {
      email: "",
      password: "",
      profile: 
      {
        title: "",
        organization: "",
        asianName: "",
        americanName: "",
        phone: ""
      }
    
    };

    onFinish = (values) => {
      this.setState({
        email: values.email,
        password: values.password,
        profile: [
          {
            title: values.job,
            organization: values.unit,
            asianName: values.chiName,
            americanName: values.engName,
            phone: values.tel
          }
        ]
      });
      // 使用者註冊
    	const { POST_register } = this.props;
    	POST_register(this.state, null);
    };
     
    
     
    
     

    render() {
      const {goToRoute} =this.props;

      const layout = {
        labelCol: {
          xs: { span: 8 },
          md: { span: 8 }, 
      },
        wrapperCol: { 
          xs: { span: 16 },
          md: { span: 16 }, },
      };
      
     
    
      return <div className="register">
              <Form
                {...layout}
                name="register"
                onFinish={this.onFinish}
              >
                <Form.Item
                  name="email"
                  label="帳號"
                  tooltip="請輸入有效信箱"
                  hasFeedback
                  style={{
                    width: '50%',
                  }}
                  rules={[
                    {
                      type: 'email',
                      message: '帳號格式不正確!',
                    },
                    {
                      required: true,
                      message: '帳號尚未輸入!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="engName"
                  label="英文姓名"
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '英文姓名尚未輸入!',

                    },
                    {
                      pattern: /^[A-Za-z]+$/,
                      message: '請輸入英文!'
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="chiName"
                  label="中文姓名"
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '中文姓名尚未輸入!',

                    },
                    {
                      pattern: /^[\u4e00-\u9fa5]+$/,
                      message: '請輸入中文!'
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="password"
                  label="密碼"
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '密碼尚未輸入!',
                    },
                    {
                      pattern: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                      message: '密碼安全度太低!'
                    },
                  ]}
                  hasFeedback
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item
                  name="confirm"
                  label="確認密碼"
                  dependencies={['password']}
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '確認密碼尚未輸入!',
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue('password') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('兩次密碼不一致!'));
                      },
                    }),
                  ]}
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item
                  name="tel"
                  label="電話"
                  hasFeedback
                  style={{
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '電話尚未輸入!',

                    },
                    {
                      pattern: /^[0-9]+$/,
                      message: '請輸入0~9!'
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="unit"
                  label="單位"
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '單位尚未輸入!',

                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="job"
                  label="稱謂"
                  hasFeedback
                  style={{
                    display: 'inline-flex',
                    width: '50%',
                  }}
                  rules={[
                    {
                      required: true,
                      message: '稱謂尚未選擇!',

                    },
                  ]}
                >
                  <Select>
                    <Option value="學生">學生</Option>
                    <Option value="教授">教授</Option>
                    <Option value="副教授">副教授</Option>
                    <Option value="助理教授">助理教授</Option>
                  </Select>
                </Form.Item>                


                <Form.Item
                  className="submitRow"
                >
                  <Button type="primary" htmlType="submit"  className="submitBtn">
                    註冊
                  </Button>
                </Form.Item>
              </Form>

        
            </div>;
    }
  },
);
