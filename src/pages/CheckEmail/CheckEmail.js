/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './CheckEmail.less';
import { Result } from 'antd';



const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class CheckEmail extends Component {
    render() {

      return <div className="checkEmail" >
          <Result
                status="success"
                title="註冊成功"
                subTitle="請至信箱收取驗證信"
            />
      </div>;
    }
  },
);
