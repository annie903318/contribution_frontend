/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './FinalPaper.less';
import Step1 from '../../components/Step1/Step1';
import Step2 from '../../components/Step2/Step2';
import Step3 from '../../components/Step3/Step3';
import Step4 from '../../components/Step4/Step4';
import { Divider } from 'antd';



const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};



export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class FinalPaper extends Component {

    render() { 
        


      return <div className="finalPaper" >
        <h2 className='contentTitle'>ID：</h2>
        <p>2020112601</p>
        <Divider />
     
        
        <Step4 />


      </div>;
    }
  },
);
