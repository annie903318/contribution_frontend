/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Upload.less';
import { Row, Col, Steps, Button, message } from 'antd';


const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};



export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Upload extends Component {

     

    render() { 
      const {goToRoute} =this.props;


      return <div className="upload" >
        <div className="uploadIntro">
          包容性語言的使用<br/>
          我們鼓勵所有作者選擇包容性和尊重性的語言。<br/> 
          提交的內容不應包含任何可能暗示一個人在種族、性別、文化、性取向或任何其他特徵方面優於另一個人的內容，<br/>
          並且應始終使用包容性語言。 <br/>
          作者應確保寫作沒有偏見，<br/>
          例如：使用“他或她”、“他或她的”、“他或她”或單數性別中立的“他們、他們或他們”。<br/>
          有關我們的包容性語言政策的更多信息， 請參見作者指南。
        </div>
        
        <Row justify="center">
          <Button type="primary" className="uploadBtn" onClick={()=>{goToRoute('/start')}}>
            開始提交
          </Button>
        </Row>




      </div>
    }
  },
);
