/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Forgot.less';
import { Form, Input, Button } from 'antd';
import logo from '@/assets/images/NUTC_logo.png'


const mapStateToProps = (state) => {
  return {
		resetRes: state.member.resetRes
	};};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
    POST_reset(payload,callback,loading) {
			dispatch({ type: 'member/POST_reset', payload,callback,loading});
		},
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Forgot extends Component {
    state = {
      email: "",
    };
    render() {     

      const onFinish = (values) => {
        this.setState({
          email: values.email,
        });
        // 忘記密碼
        const { POST_reset } = this.props;
        POST_reset(this.state, null);
      };


      return <div className="forgot" >


            <div className="logo">
              <img src={logo} alt="logo"/>
            </div>
            <div className="forgotText">
              忘記密碼
            </div>

            <Form
                name="normal_forgot"
                className="forgot-form"
                onFinish={onFinish}
              >
                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: '請輸入帳號!',
                    },
                    {
                      type: 'email',
                      message: '帳號格式不正確!',
                    },
                  ]}
                >
                  <Input placeholder="帳號" />
                </Form.Item>
              

                <Form.Item className="forgotBtn">
                  <Button type="primary" htmlType="submit" className="forgot-form-button">
                    送出
                  </Button>
                  
                </Form.Item>
              </Form>

        
      </div>;
    }
  },
);
