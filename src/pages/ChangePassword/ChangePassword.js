/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './ChangePassword.less';
import { Form, Input, Button } from 'antd';


const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
    PATCH_password(payload,callback,loading) {
			dispatch({ type: 'member/PATCH_password',payload,callback,loading});
		},
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class ChangePassword extends Component {
    state = {
      password: "",
      changePassword:""
    };
    render() {     

      const onFinish = (values) => {
        this.setState({
          password: values.password,
          changePassword: values.changePassword
        });
        // 修改密碼
        const { PATCH_password } = this.props;
        PATCH_password(this.state, null);
      };


      return <div className="changePassword" >

            <div className="changeText">
              修改密碼
            </div>

            <Form
                name="normal_changePassword"
                className="changePassword-form"
                onFinish={onFinish}
              >
                <Form.Item
                  name="password"
                  label="舊密碼"
                  rules={[
                    {
                      required: true,
                      message: '密碼尚未輸入!',
                    },
                  ]}
                  hasFeedback
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item
                  name="changePassword"
                  label="新密碼"
                  rules={[
                    {
                      required: true,
                      message: '密碼尚未輸入!',
                    },
                    {
                      pattern: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                      message: '密碼安全度太低!'
                    },
                  ]}
                  hasFeedback
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item
                  name="confirmPassword"
                  label="確認新密碼"
                  dependencies={['changePassword']}
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: '確認密碼尚未輸入!',
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue('changePassword') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('兩次密碼不一致!'));
                      },
                    }),
                  ]}
                >
                  <Input.Password />
                </Form.Item>
              

                <Form.Item className="changeBtn">
                  <Button type="primary" htmlType="submit" className="change-form-button">
                    修改
                  </Button>
                  
                </Form.Item>
              </Form>

        
      </div>;
    }
  },
);
