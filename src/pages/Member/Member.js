/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Member.less';
import PersonalPaper from '../../components/PersonalPaper/PersonalPaper';
import CooperatePaper from '../../components/CooperatePaper/CooperatePaper';
import FinalPaper from '../../components/FinalPaper/FinalPaper';
import { Tabs,Button,Row } from 'antd';
const { TabPane } = Tabs;



const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};





export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Member extends Component {
    render() {     

      return <div className="member" >

        <Tabs defaultActiveKey="1" tabPosition='top'>
            <TabPane tab={<div className='tabList'>初稿</div>} key='1'>
              <PersonalPaper/>
            </TabPane>
            {/* <TabPane tab={<div className='tabList'>終稿</div>} key="5">
              <FinalPaper />
            </TabPane> */}
            <TabPane tab={<div className='tabList'>共同稿件</div>} key="3">
              <CooperatePaper />
            </TabPane>
            
        </Tabs>
        
      </div>;
    }
  },
);
