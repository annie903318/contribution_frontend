/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './verifyDone.less';
import { Result } from 'antd';



const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class VerifyDone extends Component {
    state={
      sec:5
    }
    counter() {
      this.setState({sec:this.state.sec-1});
    }
  
    componentDidMount() {
      this.interval = setInterval(() => this.counter(), 1000);
      this.timeout = setTimeout(() => this.props.goToRoute('/member'), 5000);
    }
       
    componentWillUnmount() {
      clearInterval(this.interval);
      clearTimeout(this.timeout);
    }
      
    render() {
      
      return <div className="verifyDone" >
          <Result
                title="已驗證通過"
                subTitle={[this.state.sec,"秒後將自動跳轉"]}
            />
      </div>;
    }
  },
);
