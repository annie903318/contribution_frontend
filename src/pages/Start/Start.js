/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Start.less';

import { Row, Col, Steps, Button, message, Form, Input, Radio, Divider, Select, Upload } from 'antd';
import { UploadOutlined, PlusOutlined } from '@ant-design/icons';
import { useContext, useState, useEffect, useRef } from 'react';
import { Table, Popconfirm } from 'antd';
import { ConfigProvider, Tag, Tooltip } from 'antd';
import zhTW from 'antd/es/locale/zh_TW';
const { Step } = Steps;
const { Option } = Select;
const EditableContext = React.createContext(null);



const mapStateToProps = (state) => {
  return {
    subject:state.papers.subject || []
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
    GET_subject(callback,loading){
      dispatch({ type: 'papers/GET_subject', callback,loading });
    },
  };
};



export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Start extends Component {
    componentDidMount = () => {
      const { GET_subject } = this.props;
      const loading = (bool) => this.setState({ loading: bool });
      GET_subject(null, loading);
    };

    state = {
      current: 0,
      subject:'',
      title:'',
      abstract:'',
      keywords:[],
      fileList: [],
      cooperList: [
        {
          sort: '1',
          email: 'aa@gmail.com',
          author: '王曉明',
          engName:'Xiao-Min',
          institution: '台中科技大學',
          type:'學生',
        },
      ],
      cooperAuthorId: 1,
      // keywords
      inputVisible: false,
      inputValue: '',
      editInputIndex: -1,
      editInputValue: '',
    };

    

    // STEP1

    //檢查
    step1Check = () => {
      let {subject,title,abstract,keywords}=this.state;
      let res="";
      if(!subject){
        res+="主題尚未選擇\n";
      }
      if(!title){
        res+="標題尚未輸入\n";
      }
      if(!abstract){
        res+="摘要尚未輸入\n";
      }
      if(!keywords){
        res+="關鍵字尚未輸入\n";
      }
      if(res){
        // alert(res);
        // this.setState({current:0});
      }
    };

    // 關鍵字tags
    //tag刪除
    handleClose = removedTag => {
      const keywords = this.state.keywords.filter(tag => tag !== removedTag);
      this.setState({ keywords });
    };
    //tag新增
    showInput = () => {
      this.setState({ inputVisible: true }, () => this.input.focus());
    };
  
    handleInputChange = e => {
      this.setState({ inputValue: e.target.value });
    };
  
    handleInputConfirm = () => {
      const { inputValue } = this.state;
      let { keywords } = this.state;
      if (inputValue && keywords.indexOf(inputValue) === -1) {
        keywords = [...keywords, inputValue];
      }
      this.setState({
        keywords,
        inputVisible: false,
        inputValue: '',
      });
    };
  
    handleEditInputChange = e => {
      this.setState({ editInputValue: e.target.value });
    };
  
    handleEditInputConfirm = () => {
      this.setState(({ keywords, editInputIndex, editInputValue }) => {
        const newTags = [...keywords];
        newTags[editInputIndex] = editInputValue;
  
        return {
          keywords: newTags,
          editInputIndex: -1,
          editInputValue: '',
        };
      });
    };
  
    saveInputRef = input => {
      this.input = input;
    };
  
    saveEditInputRef = input => {
      this.editInput = input;
    };
  

    // STEP2

    //檢查
    step2Check = () => {};

    //upload設定
    fileProps = {
      multiple: false,
      // 上傳前檔案檢查
      beforeUpload: file => {
        const isPDF = file.type === 'application/pdf';
        if (!isPDF) {
          message.error('只可上傳PDF檔!');
          return Upload.LIST_IGNORE;
        }
        if (file.size > 1024*1024*25){
          message.error(`${file.name} 超過限制的檔案大小`);
          return Upload.LIST_IGNORE;
        }
        this.setState(state => ({
            fileList: [...state.fileList,file],
        }));
      },
      // 刪除
      onRemove: file => {
        this.setState({fileList: this.state.fileList.filter((item) => { 
          return item !== file; 
        })});
      }
    };


    // STEP3

    //檢查
    step3Check = () => {
    
      console.log('Step3: ', this.state);
      let res="";
      if(res){
        alert("尚有欄位未輸入");
        this.setState({current:2});
      }
    };

    //表頭
    columns = [
      {
        title: '排序',
        dataIndex: 'sort',
        editable: false,
      },
      {
        title: '作者中文姓名',
        dataIndex: 'author',
        editable: true,
      },
      {
        title: '作者英文姓名',
        dataIndex: 'engName',
        editable: true,
      },
      {
        title: '機構',
        dataIndex: 'institution',
        editable: true,
      },
      {
        title: '職稱',
        dataIndex: 'type',
        render: (text,row,index) => 
            this.state.current===3?
            <Select disabled>
              <Option value="學生">學生</Option>
              <Option value="教授">教授</Option>
              <Option value="副教授">副教授</Option>
              <Option value="助理教授">助理教授</Option>
            </Select>
            :<Select
              value={this.state.cooperList[index].type}
              onChange={(e)=>{
                
                // 取得全部cooperList
                const newData = [...this.state.cooperList];
                // 由index判斷第幾列
                // 修改職稱
                newData[index].type=e;
                // 修改完存回去 
                this.setState({
                  cooperList: newData,
                });
            }}>
              <Option value="學生">學生</Option>
              <Option value="教授">教授</Option>
              <Option value="副教授">副教授</Option>
              <Option value="助理教授">助理教授</Option>
            </Select>,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        editable: true,
      },
      {
        title: '操作',
        dataIndex: 'operation',
        render: (_, record) =>
          this.state.cooperList.length >= 1 ? (
            <Popconfirm title="確定刪除?" onConfirm={() => this.handleDelete(record.key)}>
              <a>刪除</a>
            </Popconfirm>
          ) : null,
      },
    ];
    
    //刪除列
    handleDelete = (key) => {
      const cooperList = [...this.state.cooperList];
      this.setState({
        cooperList: cooperList.filter((item) => item.key !== key),
      });
    };
    //新增一列
    handleAdd = () => {
      const { cooperAuthorId, cooperList } = this.state;
      // 檢查前一列是否都有填寫
      let prevRow=cooperList[cooperAuthorId-1];
      let prevEmpty=false;
      Object.keys(prevRow).map((item,index) => {
        if(!prevRow[item]){
          message.error(`${this.columns[index].title} 尚未填寫`);
          prevEmpty=true;
        }
      })
      // 前一列都有填寫才可新增
      if(!prevEmpty){
         const newData = {
          sort: `${cooperAuthorId+1}`,
          author: ``,
          engName:'',
          institution: '',
          type: '',
          email: '',
        };
        this.setState({
          cooperList: [...cooperList, newData],
          cooperAuthorId: cooperAuthorId + 1,
        });
      }
    };
    // 編輯新增列的內容
    handleSave = (row) => {  
      const newData = [...this.state.cooperList];
      const index = newData.findIndex((item) => row.sort === item.sort);
      const item = newData[index];

      newData.splice(index, 1, { ...item, ...row });
      this.setState({
        cooperList: newData,
      });
    };
     
    // Step3 欄位hover
    EditableRow = ({ index, ...props }) => {
      const [form] = Form.useForm();
      return (
        <Form form={form} component={false}>
          <EditableContext.Provider value={form}>
            <tr {...props} />
          </EditableContext.Provider>
        </Form>
      );
    };
    
    EditableCell = ({
      title,
      editable,
      children,
      dataIndex,
      record,
      handleSave,
      ...restProps
    }) => {
      const [editing, setEditing] = useState(false);
      const inputRef = useRef(null);
      const form = useContext(EditableContext);
      useEffect(() => {
        if (editing) {
          inputRef.current.focus();
        }
      }, [editing]);
    
      const toggleEdit = () => {
        setEditing(!editing);
        form.setFieldsValue({
          [dataIndex]: record[dataIndex],
        });
      };
    
      const save = async () => {
        try {
          const values = await form.validateFields();
          toggleEdit();
          handleSave({ ...record, ...values });
        } catch (errInfo) {
          console.log('Save failed:', errInfo);
        }
      };
    
      let childNode = children;
    
      if (editable) {
        childNode = editing ? (
          <Form.Item
            style={{
              margin: 0,
            }}
            name={dataIndex}
            rules={[
              {
                required: true,
                message: `${title} 尚未填寫`,
              },
            ]}
          >
            <Input ref={inputRef} onPressEnter={save} onBlur={save} />
          </Form.Item>
        ) : (
          <div
            className="editable-cell-value-wrap"
            style={{
              paddingRight: 24,
            }}
            onClick={toggleEdit}
          >
            {children}
          </div>
        );
      }
    
      return <td {...restProps}>{childNode}</td>;
    };
    
    
    components = {
      body: {
        row: this.EditableRow,
        cell: this.EditableCell,
      },
    };
    columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });        
  
   



    //每步驟畫面
    showContent = (current) => {
      const { subject } = this.props;
      switch (current) {
      	case 0:{
      		return <div>
                    <h2 className='contentTitle'>Step1：主題、標題、摘要、關鍵字</h2>
                    
                    <Form
                      layout='vertical'
                      >
                        
                        <Form.Item label="主題：">
                          <Select
                            placeholder=""
                            id="subject"
                            value={this.state.subject}
                            onChange={(e)=>this.setState({subject:e})}
                          >
                            {
                              subject.map((item,index)=>(
                                <Option value={item.title}>{item.title}</Option>
                              ))
                              }
                            
                          </Select>
                        </Form.Item>
                        <Divider />
                        <Form.Item label="標題：">
                          <Input id="title" placeholder=""  value={this.state.title} onChange={(e)=>this.setState({title:e.target.value})} />
                        </Form.Item>
                        <Divider />
                        <Form.Item label="摘要：">
                          <Input id="abstract" placeholder=""  value={this.state.abstract} onChange={(e)=>this.setState({abstract:e.target.value})} />
                        </Form.Item>
                        <Divider />
                        <Form.Item label="關鍵字：">
                          
                          {this.state.keywords.map((tag, index) => {
                            if (this.state.editInputIndex === index) {
                              return (
                                <Input
                                  ref={this.saveEditInputRef}
                                  key={tag}
                                  size="small"
                                  className="tag-input"
                                  value={this.state.editInputValue}
                                  onChange={this.handleEditInputChange}
                                  onBlur={this.handleEditInputConfirm}
                                  onPressEnter={this.handleEditInputConfirm}
                                />
                              );
                            }

                            const isLongTag = tag.length > 20;

                            const tagElem = (
                              <Tag
                                className="edit-tag"
                                key={tag}
                                closable
                                onClose={() => this.handleClose(tag)}
                              >
                                <span
                                  onDoubleClick={e => {
                                    if (index !== 0) {
                                      this.setState({ editInputIndex: index, editInputValue: tag }, () => {
                                        this.editInput.focus();
                                      });
                                      e.preventDefault();
                                    }
                                  }}
                                >
                                  {isLongTag ? `${tag.slice(0, 20)}...` : tag}
                                </span>
                              </Tag>
                            );
                            return isLongTag ? (
                              <Tooltip title={tag} key={tag}>
                                {tagElem}
                              </Tooltip>
                            ) : (
                              tagElem
                            );
                          })}
                          {this.state.inputVisible && (
                            <Input
                              ref={this.saveInputRef}
                              type="text"
                              size="small"
                              className="tag-input"
                              value={this.state.inputValue}
                              onChange={this.handleInputChange}
                              onBlur={this.handleInputConfirm}
                              onPressEnter={this.handleInputConfirm}
                            />
                          )}
                          {!this.state.inputVisible && (
                            <Tag className="site-tag-plus" onClick={this.showInput}>
                              <PlusOutlined /> 新增
                            </Tag>
                          )}
                        </Form.Item>
                      
                      </Form>

                  </div>;
      	}
        case 1:{
          this.step1Check()
      		return <div>
                    <h2 className='contentTitle'>Step2：檔案上傳</h2>
                    
                    <Form
                      layout='vertical'
                      >

                      <Form.Item label="稿件全文："
                                  name="upload"
                                  valuePropName="fileList"
                      >
                        <div>檔案大小限制25MB （檔案格式 pdf）</div>
                        <Upload 
                                listType="picture" 
                                {...this.fileProps} 
                                fileList={this.state.fileList}
                                maxCount={1} 
                                accept=".pdf"
                        >
                          <Button icon={<UploadOutlined />}>檔案上傳</Button>
                        </Upload>
                      </Form.Item>

                    </Form>

                  </div>;
      	}
        case 2:{
          this.step2Check()
      		return <div>
                    <h2 className='contentTitle'>Step3：作者、機構</h2>
                    
                    <Form
                      layout='vertical'
                      >
                        
                        <Form.Item label="作者和機構：">
                          
                            <ConfigProvider locale={zhTW}>
                            <Table
                                components={this.components}
                                rowClassName={() => 'editable-row'}
                                bordered
                                dataSource={this.state.cooperList}
                                columns={this.columns}
                                scroll={{ x: 800 }} 
                              />
                              <Button
                                onClick={this.handleAdd}
                                type="primary"
                                style={{
                                  margin: '14px auto',
                                  float: 'right',
                                }}
                              >
                                新增作者
                              </Button>
                            </ConfigProvider>

                        </Form.Item>
                      </Form>
                  </div>;
      	}
        case 3:{
          this.step3Check()
      		return <div>
                    <h2 className='contentTitle'>Step4：確認</h2>
                    
                    <Form
                      layout='vertical'
                      >
                        <h2 className='contentTitle'>主題、標題、摘要、關鍵字</h2>
                        <Form.Item label="主題：">
                          <Select
                            placeholder="請選擇"
                            disabled="true"
                            value={this.state.subject}
                          >
                            {
                              subject.map((item,index)=>(
                                <Option value={item.title}>{item.title}</Option>
                              ))
                            }
                          </Select>
                        </Form.Item>
                        <Divider />
                        <Form.Item label="標題：">
                          <Input placeholder="" value={this.state.title} disabled="true"/>
                        </Form.Item>
                        <Divider />
                        <Form.Item label="摘要：">
                          <Input placeholder="" value={this.state.abstract} disabled="true"/>
                        </Form.Item>
                        <Divider />
                        <Form.Item label="關鍵字：">
                          <Input.TextArea placeholder="" value={this.state.keywords} disabled="true"/>
                        </Form.Item>


                        <h2 className='contentTitle'>檔案上傳</h2>
                        <Form.Item label="稿件全文："
                                  name="upload"
                                  valuePropName="fileList"
                        >
                          <div>檔案大小限制25MB （檔案格式 pdf）</div>
                          <Upload 
                                  listType="picture" 
                                  {...this.fileProps} 
                                  fileList={this.state.fileList}
                                  maxCount={1} 
                                  accept=".pdf"
                                  disabled="true"
                          >
                            <Button icon={<UploadOutlined />}>檔案上傳</Button>
                          </Upload>
                        </Form.Item>


                        <h2 className='contentTitle'>作者、機構</h2>
                        <Form.Item label="作者和機構：">
                          
                          <ConfigProvider locale={zhTW}>
                          <Table
                              dataSource={this.state.cooperList}
                              columns={this.columns.filter((item,index)=>{
                                 return index!==this.columns.length-1;
                              })}
                              scroll={{ x: 800 }} 
                              disabled
                            />
                          </ConfigProvider>

                        </Form.Item>
                        
                      
                    </Form>
                  </div>;
      	}
        default:{
          break;
        }
      }
      
    }

     

    render() { 
      //每步驟的標題
      const steps = [
        {
          title: '主題、標題、摘要、關鍵字',
        },
        {
          title: 'Step2：檔案上傳',
        },
        {
          title: 'Step3：作者、機構',
        },
        {
          title: 'Step4：確認',
        },
      ];

      //當前步驟
      const {current} = this.state;
      
      const next = () => {
        this.setState({current:current + 1});
      };

      const prev = () => {
        this.setState({current:current - 1});
      };

      





      return <div className="start" >
     
        <Row>
          {/* 左邊 */}
          <Col xs={24} md={7}>
            <Steps direction="vertical" current={current}>
              {steps.map(item => (
                <Step key={item.title} title={item.title} />
              ))}
            </Steps>
          </Col>
          
          {/* 右邊 */}
          <Col xs={24} md={17}>
            <div className="steps-content">
              {this.showContent(current)}    
            </div>
          </Col>
        </Row>
       
        {/* 上下步驟按鈕 */}
        <div className="steps-action">
          {current > 0 && (
            <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
              上一步
            </Button>
          )}
          {current < steps.length - 1 && (
            <Button type="primary" onClick={() => next()}>
              下一步
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button type="primary" onClick={() => message.success('提交成功!')}>
              完成
            </Button>
          )}
        </div>
        

        
      </div>;
    }
  },
);
