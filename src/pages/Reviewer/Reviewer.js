/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Reviewer.less';
import ReviewPaper from '../../components/ReviewPaper/ReviewPaper';
import { Tabs } from 'antd';
const { TabPane } = Tabs;

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Reviewer extends Component {
    render() {     

      return <div className="reviewer" >
     
        <Tabs defaultActiveKey="1" tabPosition='top'>
            <TabPane tab={<div className='tabList'>所有評閱</div>} key='1'>
              <ReviewPaper />
            </TabPane> 
        </Tabs>
        
      </div>;
    }
  },
);
