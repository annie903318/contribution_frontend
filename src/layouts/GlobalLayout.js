import React, { Component } from 'react';
import withRouter from 'umi/withRouter';
import { connect } from 'dva';
import Navbar from '../components/Navbar/Navbar';
import { Layout } from 'antd';
import './GlobalLayout.less';

const { Header, Content, Footer } = Layout;

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(
    class GlobalLayout extends Component {
      state = {
        isMobile: false,
      };

      componentDidMount = () => {
        this.handleCheckIsMobile();

        window.addEventListener('resize', this.handleCheckIsMobile);
        return () => {
          window.removeEventListener('resize', this.handleCheckIsMobile);
        };
      };

      handleCheckIsMobile = () => {
        if (window.screen.width > 768) this.setState({ isMobile: false });
        else this.setState({ isMobile: true });
      };

      render() {
        const { children } = this.props;

        return (
          <Layout className="global-layout">
            <Navbar/>
            <Content  className="content">
              <div  className="content-child">{children}</div>
            </Content>
            <Footer className="footer">Copyright © 2021 國立臺中科技大學 National Taichung University of Science and Technology</Footer>
          </Layout>
        );
      }
    },
  ),
);
