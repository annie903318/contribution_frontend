/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Step2.less';
import { Steps, Form, Input, Radio, Divider, Select, Upload, Button, message  } from 'antd';
import { UploadOutlined } from '@ant-design/icons';




const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};



export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Step2 extends Component {
    state = {
      fileList: [
        
      ],
      fileList2: [
        
      ],
    };

    handleChange = info => {
      let fileList = [...info.fileList];
  
      // 1. Limit the number of uploaded files
      // Only to show two recent uploaded files, and old ones will be replaced by the new
      // fileList = fileList.slice(-2);
  
      // 2. Read from response and show file link
      fileList = fileList.map(file => {
        if (file.response) {
          // Component will show file.url as link
          file.url = file.response.url;
        }
        return file;
      });
  
      this.setState({ fileList });
    };



    render() { 
       
        const onFinish = (values) => {
            console.log('Received values of registerForm: ', values);
        };

        const props = {
          beforeUpload: file => {
            if (file.size > 1024*1024*25){
              message.error(`${file.name} 超過限制的檔案大小`);
              this.setState(state => ({
                fileList: [...state.fileList],
              }));
            }else{
              this.setState(state => ({
                fileList: [...state.fileList, file],
              }));
              return true;
            }
            
          },
          onChange: info => {
            console.log(info.fileList);
          },
          multiple: true,
        };
    





      return <div className="step2" >
     
       
              
              <Form
               onFinish={onFinish}
               layout='vertical'
              >

                
                
                <Form.Item label="稿件全文：">
                  <div>檔案大小限制25MB （檔案格式 pdf）</div>
                  <Upload {...props} fileList={this.state.fileList} maxCount={1} accept=".pdf" >
                    <Button icon={<UploadOutlined />}>檔案上傳</Button>
                  </Upload>
                </Form.Item>
                <Divider />
                <Form.Item label="補充文件：">
                  <div>可將圖檔或是表格另外上傳，各檔案大小限制25MB</div>
                  <Upload {...props} fileList={this.state.fileList2}>
                    <Button icon={<UploadOutlined />}>檔案上傳</Button>
                  </Upload>
                </Form.Item>
               
                
               
              </Form>

            
         
        

        
      </div>;
    }
  },
);
