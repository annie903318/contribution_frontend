/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './CooperatePaper.less';
import { ConfigProvider } from 'antd';
import zhTW from 'antd/es/locale/zh_TW';
import { Table } from 'antd';

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class CooperatePaper extends Component {

    render() {  
      //表格欄位
      const columns = [
        {
          title: '主題',
          dataIndex: 'theme',
          sorter: {
            compare: (a, b) => a.theme.localeCompare(b.theme),
          },
        },
        {
          title: '標題',
          dataIndex: 'title',
          sorter: {
            compare: (a, b) => a.title.localeCompare(b.title),
          },
        },
        {
          title: '投稿日期',
          dataIndex: 'addDate',
          sorter: {
            compare: (a, b) =>  {
              //將日期轉換成毫秒數，用於計算大小
              let atime=new Date(a.addDate.replace(/-/g,'/')).getTime();
              let btime=new Date(b.addDate.replace(/-/g,'/')).getTime();
              return atime- btime;
            },
          },
        },
        {
          title: '最後更新時間',
          dataIndex: 'updateDate',
          sorter: {
            compare: (a, b) =>  {
              //將日期轉換成毫秒數，用於計算大小
              let atime=new Date(a.updateDate.replace(/-/g,'/')).getTime();
              let btime=new Date(b.updateDate.replace(/-/g,'/')).getTime();
              return atime- btime;
            },
          },
        },
        {
          title: '狀態',
          dataIndex: 'paperState',
          sorter: {
            compare: (a, b) => a.paperState.localeCompare(b.paperState),
          },
          render: text => <a>{text}</a>,
        },
      ];
      
      //表格資料
      let data = [
        {
          id: '1',
          paperState: '評閱中',
          title: '使用外送APP的意願',
          addDate: '2020/11/10'	,
          updateDate: '2020/11/11',
        },
        {
          id: '2',
          paperState: '未通過',
          title: '運用AI對系統開發的意願',
          addDate: '2020/10/28'	,
          updateDate: '2020/10/29' ,
        },
        {
          id: '3',
          paperState: '通過',
          title: '使用外送APP的意願',
          addDate: '2020/11/10'	,
          updateDate: '2020/11/11' ,
        },
        {
          id: '4',
          paperState: '通過',
          title: '測試',
          addDate: '2020/10/28'	,
          updateDate: '2020/11/29' ,
        }, 
      ];

    

      return <div className="cooperatePaper" >
                <ConfigProvider locale={zhTW}>
                    <Table 
                        columns={columns} 
                        dataSource={data} 
                        bordered
                        pagination={{ position: ['none', 'bottomCenter'] }}
                        scroll={{ x: 900 }} 
                    />
                </ConfigProvider>

      </div>;
    }
  },
);
