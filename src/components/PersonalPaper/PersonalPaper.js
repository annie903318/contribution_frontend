/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './PersonalPaper.less';
import { ConfigProvider } from 'antd';
import zhTW from 'antd/es/locale/zh_TW';
import { Table } from 'antd';

const mapStateToProps = (state) => {
  return {
    personalRes:state.papers.personalRes || []
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
    GET_personal(callback,loading){
      dispatch({ type: 'papers/GET_personal', callback,loading });
    },
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class PersonalPaper extends Component {
    componentDidMount = () => {
      const { GET_personal } = this.props;
      const loading = (bool) => this.setState({ loading: bool });
      GET_personal(null, loading);
    };

    render() {  
      //表格欄位
      const columns = [
        {
          title: '主題',
          dataIndex: 'theme',
          sorter: {
            compare: (a, b) => a.theme.localeCompare(b.theme),
          },
        },
        {
          title: '標題',
          dataIndex: 'title',
          sorter: {
            compare: (a, b) => a.title.localeCompare(b.title),
          },
          render: text => <a>{text}</a>,
        },
        {
          title: '投稿日期',
          dataIndex: 'addDate',
          sorter: {
            compare: (a, b) =>  {
              //將日期轉換成毫秒數，用於計算大小
              let atime=new Date(a.addDate.replace(/-/g,'/')).getTime();
              let btime=new Date(b.addDate.replace(/-/g,'/')).getTime();
              return atime- btime;
            },
          },
        },
        {
          title: '最後更新時間',
          dataIndex: 'updateDate',
          sorter: {
            compare: (a, b) =>  {
              //將日期轉換成毫秒數，用於計算大小
              let atime=new Date(a.updateDate.replace(/-/g,'/')).getTime();
              let btime=new Date(b.updateDate.replace(/-/g,'/')).getTime();
              return atime- btime;
            },
          },
        },
        {
          title: '狀態',
          dataIndex: 'paperState',
          sorter: {
            compare: (a, b) => a.paperState.localeCompare(b.paperState),
          },
        },
      ];
      
      //表格資料
      const {personalRes} = this.props;
     
      let data = personalRes.map((item,index) => {
        return {
          theme:item.title,
          paperState: (item.accept===true)?'通過':'未通過',
          title: item.name,
          addDate: item.createdAt.substring(0, 10).replace(/-/g,'/')	,
          updateDate: item.updatedAt.substring(0, 10).replace(/-/g,'/')	,
        }
      });
    
      
      //輸出資料篩選
      // const {paperStatus}=this.props;
      // data = data.filter((item)=>{
      //   return item.paperState === paperStatus; 
      // });


      return <div className="personalPaper" >
                <ConfigProvider locale={zhTW}>
                    <Table 
                        columns={columns} 
                        dataSource={data} 
                        bordered
                        pagination={{ position: ['none', 'bottomCenter'] }}
                        scroll={{ x: 900 }} 
                    />
                </ConfigProvider>

      </div>;
    }
  },
);
