/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './ReviewPaper.less';
import { ConfigProvider } from 'antd';
import zhTW from 'antd/es/locale/zh_TW';
import { Table,Button,Modal } from 'antd';

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class ReviewPaper extends Component {
    state={
      isModalVisible:false
    }

    render() {  
      //表格欄位
      const columns = [
        {
          title: '主題',
          dataIndex: 'theme',
          sorter: {
            compare: (a, b) => a.theme.localeCompare(b.theme),
          },
        },
        {
          title: '標題',
          dataIndex: 'title',
          sorter: {
            compare: (a, b) => a.title.localeCompare(b.title),
          },
        },
        {
          title: '評閱完成日期',
          dataIndex: 'reviewDate',
          sorter: {
            compare: (a, b) =>  {
              //將日期轉換成毫秒數，用於計算大小
              let atime=new Date(a.reviewDate.replace(/-/g,'/')).getTime();
              let btime=new Date(b.reviewDate.replace(/-/g,'/')).getTime();
              return atime- btime;
            },
          },
        },
        {
          title: '評閱結果',
          dataIndex: 'result',
          render: () => (
            <div>
              <Button onClick={showModal}>建議</Button>
              <Modal 
                title="評閱建議" 
                visible={this.state.isModalVisible} 
                onOk={handleOk} 
                onCancel={handleCancel}
                footer={[
                  <Button key="submit" type="primary" onClick={handleCancel}>
                    關閉
                  </Button>,
                ]}>
                <p>Some contents...</p>
              </Modal>
            </div>
          ),
        },
      ];
      
      //表格資料
      let data = [
        {
          id: '1',
          paperId: 2020111701,
          title: '運用AI對系統開發的意願',
          reviewDate: '2020/11/18',
        },
        {
          id: '2',
          paperId: 2020111001,
          title: '使用外送APP的意願	',
          reviewDate: '2020/11/11',
        },
        {
          id: '3',
          paperId: 2020102801,
          title: '運用AI對系統開發的意願',
          reviewDate: '2020/10/29',
        },
      ];

      // 評閱建議
      const showModal = () => {
        this.setState({isModalVisible:true});
      };

      const handleOk = () => {
        this.setState({isModalVisible:false});
      };

      const handleCancel = () => {
        this.setState({isModalVisible:false});
      };
    


      return <div className="reviewPaper" >
                <ConfigProvider locale={zhTW}>
                    <Table 
                        columns={columns} 
                        dataSource={data} 
                        bordered
                        pagination={{ position: ['none', 'bottomCenter'] }}
                        scroll={{ x: 900 }} 
                    />
                </ConfigProvider>

      </div>;
    }
  },
);
