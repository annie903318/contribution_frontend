import React, { Component } from 'react';
import withRouter from 'umi/withRouter';
import { connect } from 'dva';
import { Layout, Menu, Breadcrumb } from 'antd';
import { Button } from 'antd';
import './Navbar.less';

const { Header, Content, Footer } = Layout;
const { SubMenu } = Menu;

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
    logout(callback,loading) {
			dispatch({ type: 'member/logout',callback,loading});
		},
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(
    class Navbar extends Component {
      state = {
        isMobile: false,
        lang:"中文"
      };

      componentDidMount = () => {
        this.handleCheckIsMobile();

        window.addEventListener('resize', this.handleCheckIsMobile);
        return () => {
          window.removeEventListener('resize', this.handleCheckIsMobile);
        };
      };

      handleCheckIsMobile = () => {
        if (window.screen.width > 768) this.setState({ isMobile: false });
        else this.setState({ isMobile: true });
      };


      languageChange = () =>{
        if(this.state.lang==="中文"){
          this.setState({
            lang:"英文"
          })
        }else{
          this.setState({
            lang:"中文"
          })
        }
        
     }

     memberLogout = () =>{
      const { logout } = this.props;
      const loading = (bool) => this.setState({ loading: bool });
      logout(null, loading);
     }

      render() {
        const {goToRoute} =this.props;

        return (          
            <Header className='header'>
               {
                  localStorage.getItem('token')?
                    <div>
                      <span className="logo" onClick={()=>{goToRoute('/member')}}>投稿系統</span>
                      <Menu className="navbar" theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                        <Menu.Item key="1" className="navbarItem" onClick={()=>{goToRoute('/upload')}}>開始投稿</Menu.Item>
                        <Menu.Item key="2" className="navbarItem" onClick={()=>{goToRoute('/member')}}>投稿者</Menu.Item>
                        <Menu.Item key="3" className="navbarItem" onClick={()=>{goToRoute('/reviewer')}}>評閱者</Menu.Item>
                        <Menu.Item key="4" className="navbarItem" onClick={()=>{goToRoute('/memberdata')}}>個人資料</Menu.Item>
                        <Menu.Item key="5" className="navbarItem" onClick={()=>{goToRoute('/changePassword')}}>修改密碼</Menu.Item>
                        {/* <Menu.Item key="6" className="navbarItem" onClick={this.languageChange}>{this.state.lang}</Menu.Item> */}
                        <Button type="primary" className="logout" onClick={()=>this.memberLogout()}>登出</Button>
                      </Menu>
                    </div>                   
              
                  :<span className="logo" onClick={()=>{goToRoute('/')}}>投稿系統</span>
                }
              
            </Header>
            
        );
      }
    },
  ),
);
