/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Step4.less';
import Step1 from '../../components/Step1/Step1';
import Step2 from '../../components/Step2/Step2';
import Step3 from '../../components/Step3/Step3';
import { Divider } from 'antd';


const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};



export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Step4 extends Component {
      

    render() { 
      
      return <div className="step4" >
        <h2 className='contentTitle'>類型、標題、關鍵字、摘要</h2>
        <Step1 />
        <Divider />
        <h2 className='contentTitle'>檔案上傳</h2>
        <Step2 />
        <Divider />
        <h2 className='contentTitle'>作者、機構</h2>
        <Step3 />
       
        
      </div>;
    }
  },
);
