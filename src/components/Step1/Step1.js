/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Step1.less';
import { Steps, Form, Input, Radio, Divider, Select  } from 'antd';
const { Step } = Steps;
const { Option } = Select;



const mapStateToProps = (state) => {
  return {
    // SAVE_subject: state.paper.SAVE_subject,
    // subject:state.paper.subject
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  
  };
};



export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Step1 extends Component {

    render() { 
        const validateMessages = {
            required: '${label} 尚未輸入 !',
            types: {
                email: '${label} 格式不正確!',
                number: '${label} 請輸入數字!',
            },
            number: {
                range: '${label} 需介於 ${min} 及 ${max} 碼之間',
            },
        };

        const onFinish = (values) => {
            console.log('Received values of registerForm: ', values);
        };
    

        const radioStyle = {
            display: 'block',
            height: '30px',
            lineHeight: '30px',
        };
   
        //所有主題
        const {subject}=this.props
        


      return <div className="step1" >
     
       
              
              <Form
               onFinish={onFinish}
               validateMessages={validateMessages} 
               layout='vertical'
              >

                
                <Form.Item label="主題：">
                  <Select
                    placeholder="請選擇"
                    allowClear
                  >
                    {
                      subject.map((subject1,index)=>(
                        <Option value={subject1.title}>{subject1.title}</Option>
                      ))

                      }
                    
                    {/* <Option value="ai">{subject[0].title}</Option> */}
                    {/* <Option value="iot">IOT</Option>
                    <Option value="medical">醫療</Option> */}
                  </Select>
                </Form.Item>
                <Divider />
                <Form.Item label="TITLE：">
                  <Input placeholder="" />
                </Form.Item>
                <Divider />
                <Form.Item label="ABSTRACT：">
                  <Input placeholder="" />
                </Form.Item>
                <Divider />
                <Form.Item label="Keywords：">
                  <Input.TextArea placeholder="" />
                </Form.Item>
                <Divider />
                <Form.Item label="關鍵字：">
                  <Input.TextArea placeholder="" />
                </Form.Item>
                <Divider />
                <Form.Item label="中文標題：">
                  <Input placeholder="" />
                </Form.Item>
                <Divider />
                <Form.Item label="中文摘要：">
                  <Input.TextArea placeholder="" />
                </Form.Item>
                
               
              </Form>

            
         
        

        
      </div>;
    }
  },
);
