/* eslint-disable no-template-curly-in-string */
import React, { Component } from 'react';
import { connect } from 'dva';
import './Step3.less';
import { Steps, Form, Input, Radio, Divider, Select  } from 'antd';
import { useContext, useState, useEffect, useRef } from 'react';
import { Table, Button, Popconfirm } from 'antd';
import { ConfigProvider } from 'antd';
import zhTW from 'antd/es/locale/zh_TW';
const { Step } = Steps;
const { Option } = Select;
const EditableContext = React.createContext(null);




const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToRoute(path, callback) {
      dispatch({ type: 'global/goToRoute', path, callback });
    },
  };
};



export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class Step3 extends Component {
    //表頭
    columns = [
      {
        title: '排序',
        dataIndex: 'sort',
        editable: true,
      },
      {
        title: '作者',
        dataIndex: 'author',
        editable: true,
      },
      {
        title: '機構',
        dataIndex: 'institution',
        editable: true,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        editable: true,
      },
      {
        title: '操作',
        dataIndex: 'operation',
        render: (_, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="確定刪除?" onConfirm={() => this.handleDelete(record.key)}>
              <a>刪除</a>
            </Popconfirm>
          ) : null,
      },
    ];
    state = {
      dataSource: [
        {
          key: '0',
          sort: '1',
          author: '王曉明',
          institution: '台中科技大學',
          email: 'aa@gmail.com',
        },
      ],
      count: 1,
    };
    //刪除列
    handleDelete = (key) => {
      const dataSource = [...this.state.dataSource];
      this.setState({
        dataSource: dataSource.filter((item) => item.key !== key),
      });
    };
    //新增一列
    handleAdd = () => {
      const { count, dataSource } = this.state;
      const newData = {
        key: count,
        sort: `${count+1}`,
        author: `王曉明 ${count}`,
        institution: '台中科技大學',
        email: 'aa@gmail.com',
      };
      this.setState({
        dataSource: [...dataSource, newData],
        count: count + 1,
      });
    };
    handleSave = (row) => {
      const newData = [...this.state.dataSource];
      const index = newData.findIndex((item) => row.key === item.key);
      const item = newData[index];
      newData.splice(index, 1, { ...item, ...row });
      this.setState({
        dataSource: newData,
      });
    };


  



    render() { 
      const EditableRow = ({ index, ...props }) => {
        const [form] = Form.useForm();
        return (
          <Form form={form} component={false}>
            <EditableContext.Provider value={form}>
              <tr {...props} />
            </EditableContext.Provider>
          </Form>
        );
      };
      
      const EditableCell = ({
        title,
        editable,
        children,
        dataIndex,
        record,
        handleSave,
        ...restProps
      }) => {
        const [editing, setEditing] = useState(false);
        const inputRef = useRef(null);
        const form = useContext(EditableContext);
        useEffect(() => {
          if (editing) {
            inputRef.current.focus();
          }
        }, [editing]);
      
        const toggleEdit = () => {
          setEditing(!editing);
          form.setFieldsValue({
            [dataIndex]: record[dataIndex],
          });
        };
      
        const save = async () => {
          try {
            const values = await form.validateFields();
            toggleEdit();
            handleSave({ ...record, ...values });
          } catch (errInfo) {
            console.log('Save failed:', errInfo);
          }
        };
      
        let childNode = children;
      
        if (editable) {
          childNode = editing ? (
            <Form.Item
              style={{
                margin: 0,
              }}
              name={dataIndex}
              rules={[
                {
                  required: true,
                  message: `${title} is required.`,
                },
              ]}
            >
              <Input ref={inputRef} onPressEnter={save} onBlur={save} />
            </Form.Item>
          ) : (
            <div
              className="editable-cell-value-wrap"
              style={{
                paddingRight: 24,
              }}
              onClick={toggleEdit}
            >
              {children}
            </div>
          );
        }
      
        return <td {...restProps}>{childNode}</td>;
      };

      const { dataSource } = this.state;
      const components = {
        body: {
          row: EditableRow,
          cell: EditableCell,
        },
      };
      const columns = this.columns.map((col) => {
        if (!col.editable) {
          return col;
        }

        return {
          ...col,
          onCell: (record) => ({
            record,
            editable: col.editable,
            dataIndex: col.dataIndex,
            title: col.title,
            handleSave: this.handleSave,
          }),
        };
      });        
    
      

        




      return <div className="step3" >
     
       
              
              <Form
              //  onFinish={onFinish}
              //  validateMessages={validateMessages} 
               layout='vertical'
              >

                
                
                <Form.Item label="作者和機構：">
                    
                <div>
                <ConfigProvider locale={zhTW}>
                <Table
                    components={components}
                    rowClassName={() => 'editable-row'}
                    bordered
                    dataSource={dataSource}
                    columns={columns}
                  />
                  <Button
                    onClick={this.handleAdd}
                    type="primary"
                    style={{
                      margin: '14px auto',
                      float: 'right',
                    }}
                  >
                    新增作者
                  </Button>
                </ConfigProvider>

                 
                </div>

                
                  
                </Form.Item>
               
              </Form>

            
         
        

        
      </div>;
    }
  },
);
