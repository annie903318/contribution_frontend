import request from "../utils/request";

//取得所有主題
export function GET_subject() {
  return request.get("/subject");
}

//新增論文
export function POST_paper(payload) {
  return request.post("/paper",payload);
}

// 個人投稿論文
export function GET_personal() {
  return request.get("/paper/personal",null,{
    headers:{
      Authorization:`bearer ${localStorage.getItem('token')}`
    }
  });
}